/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © Philip Withnall 2015 <philip@tecnocode.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DFL_H
#define DFL_H

/* Core files */
#include <dunfell/dfl-event.h>
#include <dunfell/dfl-event-sequence.h>
#include <dunfell/dfl-main-context.h>
#include <dunfell/dfl-parser.h>
#include <dunfell/dfl-thread.h>
#include <dunfell/dfl-time-sequence.h>
#include <dunfell/dfl-types.h>
#include <dunfell/dfl-version.h>

#endif /* !DFL_H */
